package com.bet.model.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bet.entity.GameTeam;
import com.bet.model.repositories.GameTeamRepository;

@Service
public class GameTeamService {

	@Autowired
	private GameTeamRepository gameTeamRepository;

	public Iterable<GameTeam> listAllGameTeam() {
		return gameTeamRepository.findAll();
	}

	public Optional<GameTeam> getGameTeamById(Long id) {
		return gameTeamRepository.findById(id);
	}

	public GameTeam saveGameTeam(GameTeam gameTeam) {
		return gameTeamRepository.save(gameTeam);
	}

	public void deleteGameTeam(Long id) {
		gameTeamRepository.deleteById(id);

	}

}
