package com.bet.model.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bet.entity.CodeTable;
import com.bet.model.repositories.CodeTableRepository;

@Service
public class CodeTableService {

	@Autowired
	private CodeTableRepository codeTableRepository;
	
	public List<CodeTable> listAllCodeTable() {
		return codeTableRepository.findAll();
	}
	
	public Optional<CodeTable> getCodeTableByCodeName(String codeName) {
		return codeTableRepository.findByCodeName(codeName);
	}

	public CodeTable saveCodeTable(CodeTable codeTable) {
		return codeTableRepository.save(codeTable);
	}
	
	public void deleteCodeTable(String codeId) {
		codeTableRepository.deleteByCodeId(codeId);
	}

}
