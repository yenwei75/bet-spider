package com.bet.model.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bet.entity.Game;
import com.bet.model.repositories.GameRepository;

@Service
public class GameService {

	@Autowired
	private GameRepository gameRepository;

	public Iterable<Game> listAllGame() {
		return gameRepository.findAll();
	}

	public Optional<Game> getGameById(Long id) {
		return gameRepository.findById(id);
	}

	public Game saveGame(Game game) {
		return gameRepository.save(game);
	}

	public void deleteGame(Long id) {
		gameRepository.deleteById(id);

	}

}
