package com.bet.model.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bet.entity.Team;
import com.bet.model.repositories.TeamRepository;

@Service
public class TeamService {

	@Autowired
	private TeamRepository teamRepository;

	public Iterable<Team> listAllTeam() {
		return teamRepository.findAll();
	}

	public Optional<Team> getTeamById(Long id) {
		return teamRepository.findById(id);
	}

	public Team saveTeam(Team team) {
		return teamRepository.save(team);
	}

	public void deleteTeam(Long id) {
		teamRepository.deleteById(id);
	}

}
