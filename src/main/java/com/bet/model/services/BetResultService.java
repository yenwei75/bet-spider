package com.bet.model.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bet.entity.BetResult;
import com.bet.model.repositories.BetResultRepository;

@Service
public class BetResultService {

	@Autowired
	private BetResultRepository betResultsRepository;

	public Iterable<BetResult> listAllBetResults() {
		return betResultsRepository.findAll();
	}

	public Optional<BetResult> getBetResultsById(Long id) {
		return betResultsRepository.findById(id);
	}

	public BetResult saveBetResults(BetResult betResults) {
		return betResultsRepository.save(betResults);
	}

	public void deleteBetResults(Long id) {
		betResultsRepository.deleteById(id);
	}

}
