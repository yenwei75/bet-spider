package com.bet.model.util;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;

@Component
public class GsonUtil {

	private GsonUtil() {
	};

	public static final Gson GSON = new Gson();

	public static String toGson(Object o) {
		return GSON.toJson(o);
	}

	public static <T> T fromJson(String s, Class<T> type) {
		return GSON.fromJson(s, type);
	}
}
