package com.bet.model.repositories;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bet.entity.BetResult;



@Repository
public interface BetResultRepository extends JpaRepository<BetResult, Long> {

	//public Optional<BetResults> findByCode(String code);

}
