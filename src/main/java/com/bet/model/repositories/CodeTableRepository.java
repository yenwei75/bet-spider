package com.bet.model.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bet.entity.CodeTable;

@Repository
public interface CodeTableRepository extends JpaRepository<CodeTable, Long> {
	public Optional<CodeTable> findByCodeName(String codeName);

	public void deleteByCodeId(String codeId);
}
