package com.bet.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bet.entity.GameTeam;

@Repository
public interface GameTeamRepository extends JpaRepository<GameTeam, Long> {

}
