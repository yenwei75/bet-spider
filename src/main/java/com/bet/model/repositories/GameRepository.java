package com.bet.model.repositories;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bet.entity.Game;

@Repository
public interface GameRepository extends JpaRepository<Game, Long> {


}
