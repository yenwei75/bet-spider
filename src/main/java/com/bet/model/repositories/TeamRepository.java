package com.bet.model.repositories;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bet.entity.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {


}
