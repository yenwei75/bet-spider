package com.bet.config;

import com.bet.tasklets.TeamProcessor;
import com.bet.tasklets.BetResultProcessor;
import com.bet.tasklets.BetResultWriter;
import com.bet.tasklets.CategoriesReader;
import com.bet.tasklets.CodeTableWriter;
import com.bet.tasklets.CountryReader;
import com.bet.tasklets.GameProcessor;
import com.bet.tasklets.GameTeamProcessor;
import com.bet.tasklets.GameTeamWriter;
import com.bet.tasklets.GameWriter;
import com.bet.tasklets.Reader;
import com.bet.tasklets.TeamWriter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;

//import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.AbstractFileResolvingResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.ArrayList;

/**
 * :@EnableBatchProcessing提供用于构建批处理作业的基本配置
 */

@Configuration
//@EnableScheduling
@EnableBatchProcessing
public class JobConfig {
	private static final Logger log = LoggerFactory.getLogger(JobConfig.class);

	@Autowired
	JobLauncher jobLauncher;

	@Autowired
	private JobBuilderFactory jobs;

	@Autowired
	private StepBuilderFactory steps;
	
	@Autowired
	WebDriver webDriver;
	
	@Value("${reqUrls}")
	private String[] reqUrls;
	
	@Value("${countryCodeUrl}")
	private String countryCodeUrl;
	
	@Value("${resultCategoriesUrl}")
	private String resultCategoriesUrl;
	
	
	
//	@Scheduled(cron = "*/5 * * * * ?")//每天PM六點執行
//	public void perform() throws Exception {
//		JobParameters params = new JobParametersBuilder().addString("JobID", String.valueOf(System.currentTimeMillis()))
//				.toJobParameters();
//		jobLauncher.run(job(), params);
//	}

	@Bean
	public Job codeTablejob() {//codeTable之後只要run一次，就不用再run了。	
		return jobs.get("job").incrementer(new RunIdIncrementer())
				//讀取countryCode資料來源
				.start(readCountry(countryCodeUrl))
				//寫入code_table
//				.next(writeCodeTable())
				//讀取resultCategories資料來源
				.next(readCategories(resultCategoriesUrl))
				//寫入code_table
				.next(writeCodeTable())
				.build();
		
		//TODO webDriver.quit();
	}
	
	@Bean
	public Job job() {	
		return jobs.get("job").incrementer(new RunIdIncrementer())
				.start(read(this.reqUrls))//讀取資料來源
				//過一手來源資料，給processTeam、processGame、processGameTeam使用
				.next(processBetResult())
				.next(writeBetResult())//debug寫入table方便看資料
				//處理、寫入Team table
				.next(processTeam())
				.next(writeTeam())
				//處理、寫入Game table
				.next(processGame())
				.next((writeGame()))
				//處理、寫入game_team table
				.next(processGameTeam())
				.next((writeGameTeam()))
				.build();
	}

	

	@Bean
	@Scope("prototype")
	protected Step read(String... reqUrls) {
		return steps.get("read").tasklet(reader(reqUrls)).build();
	}
	
	@Bean
//	@Scope("prototype")
	protected Step readCountry(String reqUrl) {
		return steps.get("readCountry").tasklet(countryReader(reqUrl)).build();
	}
	
	@Bean
//	@Scope("prototype")
	protected Step readCategories(String reqUrl) {
		return steps.get("readCategories").tasklet(categoriesReader(reqUrl)).build();
	}
	
	@Bean
	protected Step processBetResult() {
		return steps.get("processBetResult").tasklet(betResultProcessor()).build();
	}
	

	@Bean
	protected Step processTeam() {
		return steps.get("processTeam").tasklet(teamProcessor()).build();
	}
	

	@Bean
	protected Step processGame() {
		return steps.get("processGame").tasklet(gameProcessor()).build();
	}

	@Bean
	protected Step processGameTeam() {
		return steps.get("processGameTeam").tasklet(gameTeamProcessor()).build();
	}

	
	@Bean
	protected Step writeCodeTable() {
		return steps.get("writeCodeTable").tasklet(CodeTableWriter()).build();
	}
	
	@Bean
	protected Step writeBetResult() {
		return steps.get("writeBetResult").tasklet(betResultWriter()).build();
	}
	
	@Bean
	protected Step writeTeam() {
		return steps.get("writeTeam").tasklet(teamWriter()).build();
	}
	
	@Bean
	protected Step writeGame() {
		return steps.get("writeGame").tasklet(gameWriter()).build();
	}
	
	@Bean
	protected Step writeGameTeam() {
		return steps.get("writeGameTeam").tasklet(gameTeamWriter()).build();
	}


//	@Bean
//	@Scope("prototype")
//	public TeamReader teamReader(String reqUrl) {
//		TeamReader teamReader = new TeamReader();
//		teamReader.setReqUrl(reqUrl);
//		return teamReader;
//	}
	
	
	@Bean
	@Scope("prototype")
	public Reader reader(String... reqUrls) {
		return new Reader(reqUrls);
	}
	
	@Bean
//	@Scope("prototype")
	public CountryReader countryReader(String reqUrl) {
		return new CountryReader(reqUrl);
	}
	
	@Bean
//	@Scope("prototype")
	public CategoriesReader categoriesReader(String reqUrl) {
		return new CategoriesReader(reqUrl);
	}
	
	
	@Bean
	public BetResultProcessor betResultProcessor() {
		return new BetResultProcessor();
	}
	
	
	@Bean
	public TeamProcessor teamProcessor() {
		return new TeamProcessor();
	}
	
	@Bean
	public GameProcessor gameProcessor() {
		return new GameProcessor();
	}
	
	@Bean
	public GameTeamProcessor gameTeamProcessor() {
		return new GameTeamProcessor();
	}


	@Bean
	public BetResultWriter betResultWriter() {
		return new BetResultWriter();
	}
	
	@Bean
	public TeamWriter teamWriter() {
		return new TeamWriter();
	}
	
	@Bean
	public GameWriter gameWriter() {
		return new GameWriter();
	}

	@Bean
	public GameTeamWriter gameTeamWriter() {
		return new GameTeamWriter();
	}
	
	@Bean
	public CodeTableWriter CodeTableWriter() {
		return new CodeTableWriter();
	}
	
	@Bean
	public WebDriver getWebDriver() {
		Resource resource = new ClassPathResource("chromedriver.exe");
		// 取得chromedriver路徑
		String path = "";
		try {
			path = ((AbstractFileResolvingResource) resource).getFile().getPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.setProperty("webdriver.chrome.driver", path);
//		System.out.println(System.getProperty("webdriver.chrome.driver"));

		ArrayList<String> command = new ArrayList<String>();
//		command.add("--headless");// 是否開啟瀏覽器執行

		ChromeOptions options = new ChromeOptions();
		options.addArguments(command);
		WebDriver webDriver = new ChromeDriver(options);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return webDriver;
	}
}
