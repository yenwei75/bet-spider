package com.bet.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "s_code_table")
@EntityListeners(AuditingEntityListener.class)
public class CodeTable {

	/* 国际域名缩写 */
	@Id
	@Column(name = "code_id", columnDefinition = "char(10)")
	private String codeId;

	/* 代碼種類 */
	@Column(name = "code_type", columnDefinition = "char(16)")
	private String codeType;

	/* 代碼名稱 */
	@Column(name = "code_name", columnDefinition = "char(20)")
	private String codeName;

	/* 代碼描述 */
	@Column(name = "`desc`", length = 256)
	private String desc;

	/* 上一階層id */
	@Column(name = "parent_code_id", columnDefinition = "char(16) COMMENT '父類 codetable id'")
	private String parentCodeId;

	/* 國家代碼 */
	@Column(name = "country_id", columnDefinition = "char(16) COMMENT 'country id'")
	private String countryId;

	@Override
	public int hashCode() {
		return Objects.hash(codeName, codeType, countryId, desc, parentCodeId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodeTable other = (CodeTable) obj;
		return Objects.equals(codeName, other.codeName) && Objects.equals(codeType, other.codeType)
				&& Objects.equals(countryId, other.countryId) && Objects.equals(desc, other.desc)
				&& Objects.equals(parentCodeId, other.parentCodeId);
	}

	public String getCodeId() {
		return codeId;
	}

	public void setCodeId(String codeId) {
		this.codeId = codeId;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getParentCodeId() {
		return parentCodeId;
	}

	public void setParentCodeId(String parentCodeId) {
		this.parentCodeId = parentCodeId;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	@Override
	public String toString() {
		return "CodeTable [codeId=" + codeId + ", codeType=" + codeType + ", codeName=" + codeName + ", desc=" + desc
				+ ", parentCodeId=" + parentCodeId + ", countryId=" + countryId + "]";
	}

}
