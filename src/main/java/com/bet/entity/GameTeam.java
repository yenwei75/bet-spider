package com.bet.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "g_game_team")
@EntityListeners(AuditingEntityListener.class)
public class GameTeam {

	@OneToOne(targetEntity = Game.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "game_id", referencedColumnName = "game_id", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "FK_Game_GameTeam")) // 注释本表中指向另一个表的外键。
	private Game game;

	@OneToOne(targetEntity = Team.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "team_id", referencedColumnName = "team_id", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "FK_Team_GameTeam")) // 注释本表中指向另一个表的外键。
	private Team team;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "game_team_id", nullable = false, columnDefinition = "int(10)")
	private int gameTeamId;

	@Column(name = "sort", nullable = false, columnDefinition = "int(2) COMMENT '比賽隊伍排序 1:主隊,2:客隊'")
	private String sort;

	@Column(name = "score", columnDefinition = "char(3) COMMENT '得分'")
	private String score;

	@Column(name = "game_id", columnDefinition = "int(10)")
	private int gameId;

	@Column(name = "team_id", columnDefinition = "int(10)")
	private int teamId;

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public int getGameTeamId() {
		return gameTeamId;
	}

	public void setGameTeamId(int gameTeamId) {
		this.gameTeamId = gameTeamId;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	@Override
	public String toString() {
		return "GameTeam [game=" + game + ", team=" + team + ", gameTeamId=" + gameTeamId + ", sort=" + sort
				+ ", score=" + score + ", gameId=" + gameId + ", teamId=" + teamId + "]";
	}

}
