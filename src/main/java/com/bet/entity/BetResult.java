package com.bet.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "bet_result")
@EntityListeners(AuditingEntityListener.class)
public class BetResult {
	// TODO加comment
	/* 流水號 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sn", length = 10)
	private Long sn;

	@Column(name = "big_category", columnDefinition = "char(4) COMMENT '競技大類代碼(球類、金融)'")
	private String bigCategory;

	@Column(name = "game_kind", columnDefinition = "char(20) COMMENT '賽事種類(棒球、籃球、網球等..)'")
	private String gameKind;

	@Column(name = "league_name", columnDefinition = "char(100) COMMENT '聯盟名稱'")
	private String leagueName;

	@Column(name = "country", columnDefinition = "char(100) COMMENT '國家'")
	private String country;

	@Column(name = "team_type", columnDefinition = "char(100) COMMENT '隊種(1:主，2:客 )'")
	private String teamType;

	@Column(name = "team_name", columnDefinition = "char(50) COMMENT '隊名(繁体)'")
	private String teamName;

	@Column(name = "cn_team_name", columnDefinition = "char(50) COMMENT '隊名(簡體)'")
	private String cnTeamName;

	@Column(name = "start_time", columnDefinition = "DATETIME COMMENT '比賽時間'")
	private Date startTime;

	/* 投注狀態 */
	@Column(name = "status", columnDefinition = "char(10) COMMENT '投注狀態:completed  ,cancelled'")   
	private String status;

	@Column(name = "code", columnDefinition = "char(10) COMMENT '賽事 編號 '")
	private String code;

	@Column(name = "eng_name", columnDefinition = "char(10) COMMENT '比賽隊伍名稱英文 '")
	private String engName;

//	/* 第一場得分 */
//	@Column(name = "round1", length = 10)
//	private String round1;
//
//	/* 第二場得分 */
//	@Column(name = "round2", length = 10)
//	private String round2;
//
//	/* 第三場得分 */
//	@Column(name = "round3", length = 10)
//	private String round3;
//
//	/* 第四場得分 */
//	@Column(name = "round4", length = 10)
//	private String round4;
//
//	/* 第五場得分 */
//	@Column(name = "round5", length = 10)
//	private String round5;
//
//	/* 第六場得分 */
//	@Column(name = "round6", length = 10)
//	private String round6;
//
//	/* 第七場得分 */
//	@Column(name = "round7", length = 10)
//	private String round7;
//
//	/* 第八場得分 */
//	@Column(name = "round8", length = 10)
//	private String round8;
//
//	/* 第九場得分 */
//	@Column(name = "round9", length = 10)
//	private String round9;
//
//	/* 延長賽得分 */
//	@Column(name = "extra_round", length = 10)
//	private String extraRound;

	@Column(name = "total_score", columnDefinition = "char(10) COMMENT '總得分'")
	private String totalScore;

	// @Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@Column(name = "update_time")
	private Date updateTime;

	public Long getSn() {
		return sn;
	}

	public void setSn(Long sn) {
		this.sn = sn;
	}

	public String getBigCategory() {
		return bigCategory;
	}

	public void setBigCategory(String bigCategory) {
		this.bigCategory = bigCategory;
	}

	public String getGameKind() {
		return gameKind;
	}

	public void setGameKind(String gameKind) {
		this.gameKind = gameKind;
	}

	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(String leagueName) {
		this.leagueName = leagueName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTeamType() {
		return teamType;
	}

	public void setTeamType(String teamType) {
		this.teamType = teamType;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getCnTeamName() {
		return cnTeamName;
	}

	public void setCnTeamName(String cnTeamName) {
		this.cnTeamName = cnTeamName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEngName() {
		return engName;
	}

	public void setEngName(String engName) {
		this.engName = engName;
	}

	public String getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "BetResult [sn=" + sn + ", bigCategory=" + bigCategory + ", gameKind=" + gameKind + ", leagueName="
				+ leagueName + ", country=" + country + ", teamType=" + teamType + ", teamName=" + teamName
				+ ", cnTeamName=" + cnTeamName + ", startTime=" + startTime + ", status=" + status + ", code=" + code
				+ ", engName=" + engName + ", totalScore=" + totalScore + ", updateTime=" + updateTime + "]";
	}

	
}
