package com.bet.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "g_game")
@EntityListeners(AuditingEntityListener.class)
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "game_id", nullable = false, columnDefinition = "int(10) COMMENT 'gameId'")
	private int gameId;

	@Column(name = "big_category", columnDefinition = "char(4) COMMENT '大分類(球類、金融)'")
	private String bigCategory;

	@Column(name = "mid_category", nullable = false, columnDefinition = "char(4) COMMENT '中分類(足球、棒球)'")
	private String midCategory;

	@Column(name = "small_category", nullable = false, columnDefinition = "char(20) COMMENT '小分類(歐洲聯賽冠軍盃、瑞典超級聯賽)'")
	private String smallCategory;

	@Column(name = "code", columnDefinition = "char(6) COMMENT '賽事編號'")
	private String code;

	@Column(name = "game_time", columnDefinition = "DATETIME COMMENT '比賽時間'")
	private Date gameTime;

	@Column(name = "country", columnDefinition = "char(20) COMMENT '國家'")
	private String country;

	@UpdateTimestamp
	@Column(name = "last_upt_date", columnDefinition = "DATETIME COMMENT '最後修改時間'")
	private Date lastUptDate;

	@CreatedDate
	@Column(name = "create_time", columnDefinition = "DATETIME COMMENT '建立時間'")
	private Date createTime;

	@Column(name = "last_upt_user", nullable = false, columnDefinition = "int(10) COMMENT '最後修改者'")
	private String lastUptUser;

	@Column(name = "status", nullable = false, columnDefinition = "char(1) default '1' COMMENT '賽事狀態, 0:作廢; 1:正常;'")
	private String status;

	@Override
	public int hashCode() {
		return Objects.hash(code, gameTime, midCategory);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		return Objects.equals(code, other.code) && Objects.equals(gameTime, other.gameTime)
				&& Objects.equals(midCategory, other.midCategory);
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public String getBigCategory() {
		return bigCategory;
	}

	public void setBigCategory(String bigCategory) {
		this.bigCategory = bigCategory;
	}

	public String getMidCategory() {
		return midCategory;
	}

	public void setMidCategory(String midCategory) {
		this.midCategory = midCategory;
	}

	public String getSmallCategory() {
		return smallCategory;
	}

	public void setSmallCategory(String smallCategory) {
		this.smallCategory = smallCategory;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getGameTime() {
		return gameTime;
	}

	public void setGameTime(Date gameTime) {
		this.gameTime = gameTime;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getLastUptDate() {
		return lastUptDate;
	}

	public void setLastUptDate(Date lastUptDate) {
		this.lastUptDate = lastUptDate;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getLastUptUser() {
		return lastUptUser;
	}

	public void setLastUptUser(String lastUptUser) {
		this.lastUptUser = lastUptUser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Game [gameId=" + gameId + ", bigCategory=" + bigCategory + ", midCategory=" + midCategory
				+ ", smallCategory=" + smallCategory + ", code=" + code + ", gameTime=" + gameTime + ", country="
				+ country + ", lastUptDate=" + lastUptDate + ", createTime=" + createTime + ", lastUptUser="
				+ lastUptUser + ", status=" + status + "]";
	}

}
