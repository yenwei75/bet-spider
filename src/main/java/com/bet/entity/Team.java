package com.bet.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "g_team")
@EntityListeners(AuditingEntityListener.class)
public class Team {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "team_id", nullable = false, columnDefinition = "int(10) COMMENT 'teamId'")
	private int teamId;

	@Column(name = "big_category", nullable = false, columnDefinition = "char(4) COMMENT '競技大類代碼(球類、金融)'")
	private String bigCategory;

	@Column(name = "mid_category", nullable = false, columnDefinition = "char(4) COMMENT '競技中類代碼(足球、棒球)'")
	private String midCategory;

	@Column(name = "small_category", columnDefinition = "char(8) COMMENT '競技小類代碼(隊伍所屬聯盟)'")
	private String smallCategory;

	@Column(name = "eng_name", nullable = false, columnDefinition = "varchar(64) COMMENT '比賽隊伍名稱英文'")
	private String engName;

	@Column(name = "cn_ch_name", columnDefinition = "varchar(64) COMMENT '比賽隊伍名稱簡體'")
	private String cnChName;

	@Column(name = "tw_ch_name", columnDefinition = "varchar(64) COMMENT '比賽隊伍名稱繁體'")
	private String twChName;

	@Column(name = "remark", columnDefinition = "varchar(256) COMMENT '備註'")
	private String remark;


	@Override
	public int hashCode() {
		return Objects.hash(bigCategory, cnChName, engName, midCategory, twChName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		return Objects.equals(bigCategory, other.bigCategory) && Objects.equals(cnChName, other.cnChName)
				&& Objects.equals(engName, other.engName) && Objects.equals(midCategory, other.midCategory)
				&& Objects.equals(twChName, other.twChName);
	}



	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getBigCategory() {
		return bigCategory;
	}

	public void setBigCategory(String bigCategory) {
		this.bigCategory = bigCategory;
	}

	public String getMidCategory() {
		return midCategory;
	}

	public void setMidCategory(String midCategory) {
		this.midCategory = midCategory;
	}

	public String getSmallCategory() {
		return smallCategory;
	}

	public void setSmallCategory(String smallCategory) {
		this.smallCategory = smallCategory;
	}

	public String getEngName() {
		return engName;
	}

	public void setEngName(String engName) {
		this.engName = engName;
	}

	public String getCnChName() {
		return cnChName;
	}

	public void setCnChName(String cnChName) {
		this.cnChName = cnChName;
	}

	public String getTwChName() {
		return twChName;
	}

	public void setTwChName(String twChName) {
		this.twChName = twChName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "Team [teamId=" + teamId + ", bigCategory=" + bigCategory + ", midCategory=" + midCategory
				+ ", smallCategory=" + smallCategory + ", engName=" + engName + ", cnChName=" + cnChName + ", twChName="
				+ twChName + ", remark=" + remark + "]";
	}
}
