package com.bet.tasklets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bet.entity.BetResult;
import com.bet.entity.CodeTable;
import com.bet.model.services.CodeTableService;
import com.github.houbb.opencc4j.util.ZhConverterUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BetResultProcessor implements Tasklet {

	private final Logger logger = LoggerFactory.getLogger(BetResultProcessor.class);

	private List<String> jsonList = new ArrayList<String>();
	private List<BetResult> betResultList = new ArrayList<BetResult>();

	@Autowired
	private CodeTableService codeTableService;

	@Value("${bigCategory}")
	private String bigCategory;

	@Value("${emptyValue}")
	private String emptyValue;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		this.jsonList = (List<String>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("jsonList");

		List<CodeTable> codeTableList = codeTableService.listAllCodeTable();

		for (int i = 0; i < jsonList.size(); i++) {
			JsonObject jsonObj = new JsonParser().parse(jsonList.get(i)).getAsJsonObject();
			JsonArray betGameResultsArray = jsonObj.get("betGameResults").getAsJsonArray();

			for (int j = 0; j < betGameResultsArray.size(); j++) {
				JsonObject betGameResultsJsonObj = betGameResultsArray.get(j).getAsJsonObject();
				JsonObject resourcesJsonObj = jsonObj.get("lexicon").getAsJsonObject().get("resources")
						.getAsJsonObject();

				String bigCategoryId = "";
				// 球種
				String gameKind = getNameById(betGameResultsJsonObj.get("s"), resourcesJsonObj);
				String gameKindId = "";

				// (網頁欄位一)聯盟名稱
				String leagueName = getNameById(betGameResultsJsonObj.get("t"), resourcesJsonObj);
				String leagueNameId = "";
				// (網頁欄位二)開賽 時間
				Date startTime = new Date(Long.parseLong(betGameResultsJsonObj.get("d").getAsString().trim()));
				// (網頁欄位三)賽事編號
				String code = betGameResultsJsonObj.get("cd").getAsString().trim();

				// 客隊代號
				JsonElement awayTeamId = betGameResultsJsonObj.get("a");
				// 主隊代號
				JsonElement homeTeamId = betGameResultsJsonObj.get("h");

				// (網頁欄位四)客隊名
				String awayTeamName = getNameById(awayTeamId, resourcesJsonObj);
				// (網頁欄位四)主隊名
				String homeTeamName = getNameById(homeTeamId, resourcesJsonObj);

				if ("".equals(awayTeamName) && "".equals(homeTeamName)) {// 不要隊名為空的資料
					continue;
				}

				// 客隊每場得分明細
				JsonElement awayTeamEveryRoundScore = betGameResultsJsonObj.get("vsp");
				// 主隊每場得分明細
				JsonElement homeTeamEveryRoundScore = betGameResultsJsonObj.get("hpp");
				// (網頁最後欄位)(客)結果
				String awayTeamTotalScore = this.getTotalScore(awayTeamEveryRoundScore);
				// (網頁最後欄位)(主)結果
				String homeTeamTotalScore = this.getTotalScore(homeTeamEveryRoundScore);

				// 國家
				String country = getNameById(betGameResultsJsonObj.get("c"), resourcesJsonObj);
				String countryId = "";
				// (客)簡體
				String cnAwayTeamName = ZhConverterUtil.convertToSimple(awayTeamName);
				// (主)簡體
				String cnHomeTeamName = ZhConverterUtil.convertToSimple(homeTeamName);

				// 投注狀態
				String status = betGameResultsJsonObj.get("st").getAsString().trim();

				bigCategoryId = getCodeId(this.bigCategory, codeTableList);
				gameKindId = getCodeId(gameKind, codeTableList);
				countryId = getCodeId(country, codeTableList);

				for (CodeTable codeTable : codeTableList) {
					if (leagueName.equals(codeTable.getCodeName()) && gameKindId.equals(codeTable.getParentCodeId())) {// 沒有一個聯盟名稱會等於棒球，所以棒球時不會進if
						leagueNameId = codeTable.getCodeId();
						break;
					} else {
						leagueNameId = this.emptyValue;
					}
				}

//				for (CodeTable codeTable : listAllCodeTable) {
//					if (leagueName.equals(codeTable.getCodeName())) {// 沒有一個聯盟名稱會等於棒球，所以棒球時不會進if
//						CodeTable gameKindCodeTable = codeTableService.getCodeTableByCodeName(gameKind).orElse(null);
//						gameKindId = gameKindCodeTable.getCodeId();
//						if (gameKindId.equals(codeTable.getParentCodeId())) {
//							System.out.println("gameKindId=> " + gameKindId);
//							leagueNameId = codeTable.getCodeId();
//							break;
//						}else {
//							leagueNameId = this.emptyValue;
//						}
//					}
//				}

				// 分別 insert 主客隊資料
				for (int k = 1; k <= 2; k++) {
					BetResult betResult = new BetResult();
					betResult.setBigCategory(bigCategoryId);
					betResult.setLeagueName(leagueNameId);
					betResult.setStartTime(startTime);
					betResult.setCode(code);
					betResult.setGameKind(gameKindId);
					betResult.setCountry(countryId);
					betResult.setStatus(status);
					if (k == 1) { // 主隊
						betResult.setTeamType("1");// 1:主隊;2:客隊
						betResult.setTeamName(homeTeamName);
						betResult.setTotalScore(homeTeamTotalScore);
						betResult.setCnTeamName(cnHomeTeamName);
					} else {// 客隊
						betResult.setTeamType("2");// 1:主隊;2:客隊
						betResult.setTeamName(awayTeamName);
						betResult.setTotalScore(awayTeamTotalScore);
						betResult.setCnTeamName(cnAwayTeamName);
					}
					this.betResultList.add(betResult);
				}
			}
		}

		ExecutionContext executionContext = chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext();
		executionContext.put("betResultList", this.betResultList);

		return RepeatStatus.FINISHED;
	}

	private String getNameById(JsonElement id, JsonObject resourcesJsonObj) {// 用代號取得名稱
		String strId = "";
		String name = "";
		if (!id.isJsonNull()) {
			strId = id.getAsString().trim();
			name = resourcesJsonObj.get(strId).getAsString().trim();
		}
		return name;
	}

	private String getTotalScore(JsonElement everyRoundScore) {// 取得總得分
		int totalScore = 0;
		if (!everyRoundScore.isJsonNull()) {// 計算總得分
			for (int round = 1; round <= 10; round++) {
				JsonElement roundScore = everyRoundScore.getAsJsonObject().get(String.valueOf(round));
				if (!roundScore.isJsonNull()) {
					int finalRoundScore = roundScore.getAsInt();
					finalRoundScore = finalRoundScore > 0 ? finalRoundScore : 0;
					totalScore += finalRoundScore;
				}
			}
		}
		return String.valueOf(totalScore);
	}

	public String getCodeId(String value, List<CodeTable> codeTableList) {//用名稱取得代號
		String codeId = "";
		for (CodeTable codeTable : codeTableList) {
			if (value.equals(codeTable.getCodeName())) {// ex:球類
				codeId = codeTable.getCodeId();// ex:B
				break;
			} else {
				codeId = this.emptyValue;
			}
		}
		return codeId;
	}
}
