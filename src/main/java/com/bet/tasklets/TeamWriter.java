package com.bet.tasklets;

import com.bet.entity.Team;
import com.bet.model.services.TeamService;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class TeamWriter implements Tasklet {

	@Autowired
	private TeamService teamService;

	private final Logger logger = LoggerFactory.getLogger(TeamWriter.class);
	private Set<Team> teamSet = new HashSet<Team>();

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		this.teamSet = (Set<Team>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("teamSet");

		for (Team team : this.teamSet) {
			teamService.saveTeam(team);
		}
		return RepeatStatus.FINISHED;
	}

}
