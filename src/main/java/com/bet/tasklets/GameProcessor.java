package com.bet.tasklets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import org.springframework.beans.factory.annotation.Value;

import com.bet.entity.BetResult;

import com.bet.entity.Game;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GameProcessor implements Tasklet {

	private final Logger logger = LoggerFactory.getLogger(GameProcessor.class);
	private List<BetResult> betResultList = new ArrayList<BetResult>();
	private Set<Game> gameSet = new HashSet<Game>();

	@Value("${bigCategory}")
	private String bigCategory;

//	@Value("${gameType}")
//	private String gameType;// 賽事種類: 1.整場 2.上半場 3.下半場

	@Value("${lastUptUser}")
	private String lastUptUser;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		this.betResultList = (List<BetResult>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("betResultList");

		for (BetResult betResult : this.betResultList) {
			String bigCategory = betResult.getBigCategory();
			String gameKind = betResult.getGameKind();// 球種
			String leagueName = betResult.getLeagueName();// 聯盟
			String code = betResult.getCode();// 賽事編號
			String country = betResult.getCountry();// 國家
			Date startTime = betResult.getStartTime();// 開賽 時間
			String status = betResult.getStatus();//投注狀態
			if ("completed".equals(status)) {
				status = "1";
			} else {
				status = "0";
			}

			Game game = new Game();
			game.setBigCategory(bigCategory);
			game.setMidCategory(gameKind);
			game.setSmallCategory(leagueName);
			game.setCode(code);
			game.setCountry(country);
			game.setGameTime(startTime);
			game.setStatus(status);
			game.setLastUptUser(lastUptUser);
			this.gameSet.add(game);

		}

		ExecutionContext executionContext = chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext();
		executionContext.put("gameSet", this.gameSet);

		return RepeatStatus.FINISHED;
	}

}
