package com.bet.tasklets;

import com.bet.entity.CodeTable;
import com.bet.model.services.CodeTableService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class CodeTableWriter implements Tasklet {

	@Autowired
	private CodeTableService codeTableService;

	private final Logger logger = LoggerFactory.getLogger(CodeTableWriter.class);
	private Set<CodeTable> codeTableSet = new HashSet<CodeTable>();

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		this.codeTableSet = (Set<CodeTable>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("codeTableSet");

		for (CodeTable codeTable : this.codeTableSet) {
			codeTableService.saveCodeTable(codeTable);
		}
		return RepeatStatus.FINISHED;
	}

}
