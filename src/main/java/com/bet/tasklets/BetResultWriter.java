package com.bet.tasklets;

import com.bet.entity.BetResult;

import com.bet.model.services.BetResultService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

import java.util.List;

public class BetResultWriter implements Tasklet {

	@Autowired
	BetResultService betResultService;

	private final Logger logger = LoggerFactory.getLogger(BetResultWriter.class);
	private List<BetResult> betResultList = new ArrayList<BetResult>();

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		this.betResultList = (List<BetResult>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("betResultList");

		for (BetResult betResult : this.betResultList) {
			betResultService.saveBetResults(betResult);
		}
		return RepeatStatus.FINISHED;
	}

}
