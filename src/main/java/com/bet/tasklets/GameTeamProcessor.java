package com.bet.tasklets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.bet.entity.BetResult;
import com.bet.entity.Game;
import com.bet.entity.GameTeam;
import com.bet.entity.Team;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GameTeamProcessor implements Tasklet {

	private final Logger logger = LoggerFactory.getLogger(GameTeamProcessor.class);

	private List<BetResult> betResultList = new ArrayList<BetResult>();
	private Set<GameTeam> gameTeamSet = new HashSet<GameTeam>();
	private Set<Game> gameSet = new HashSet<Game>();
	private Set<Team> teamSet = new HashSet<Team>();

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		this.betResultList = (List<BetResult>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("betResultList");

		this.gameSet = (Set<Game>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("gameSet");

		this.teamSet = (Set<Team>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("teamSet");

		for (BetResult betResult : this.betResultList) {
			String sort = betResult.getTeamType();
			String score = betResult.getTotalScore();

			String leagueName = betResult.getLeagueName();
			String code = betResult.getCode();
			Date startTime = betResult.getStartTime();
			int gameId = 0;
			int teamId = 0;
			String gameKind = betResult.getGameKind();
			String teamName = betResult.getTeamName();

			for (Game game : this.gameSet) {// left join game的key
				if (leagueName.equals(game.getSmallCategory()) && code.equals(game.getCode())
						&& startTime.equals(game.getGameTime())) {
					gameId = game.getGameId();
//					System.out.println(game.toString());
				}

			}

			for (Team team : this.teamSet) {// left join team的key
				if (gameKind.equals(team.getMidCategory()) && teamName.equals(team.getTwChName())) {
					teamId = team.getTeamId();
//					System.out.println(team.toString());
				}

			}
			GameTeam gameTeam = new GameTeam();

			gameTeam.setSort(sort);
			gameTeam.setScore(score);
			gameTeam.setGameId(gameId);
			gameTeam.setTeamId(teamId);
			this.gameTeamSet.add(gameTeam);
		}

		ExecutionContext executionContext = chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext();
		executionContext.put("gameTeamSet", this.gameTeamSet);

		return RepeatStatus.FINISHED;
	}

}
