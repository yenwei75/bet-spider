package com.bet.tasklets;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class Reader implements Tasklet {

	@Autowired
	WebDriver webDriver;

	private final Logger logger = LoggerFactory.getLogger(Reader.class);

	private List<String> jsonList = new ArrayList<String>();

	private String[] reqUrls;// 爬取的網址
	private By locator;

	public Reader(String... reqUrls) {
		super();
		this.reqUrls = reqUrls;
	}

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

		for (String reqUrl : this.reqUrls) {
			getHtmlText(reqUrl);
		}
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("jsonList",
				this.jsonList);

		return RepeatStatus.FINISHED;
	}

	private void getHtmlText(String reqUrl) {
		int currentPage = 1;
		int totalPage;
		do {
			webDriver.get(reqUrl);
//			System.out.println("reqUrl=>" + reqUrl);
			String finalReqUrl = reqUrl + "&page=" + currentPage;
//			System.out.println("finalReqUrl=>" + finalReqUrl);

//			System.out.println("【開始抓取第" + currentPage + "頁資料!】");
			webDriver.get(finalReqUrl);
			this.locator = By.xpath("/html/body/pre");// 爬取資料的xpath路徑
			WebDriverWait wait = new WebDriverWait(webDriver, 10, 1); // 每1秒檢查元素是否存在，等待10秒直到元素可见。
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			WebElement element = webDriver.findElement(locator);
//			System.out.println("==========爬取資料如下==========:");
			String htmlText = element.getText().trim();
//			System.out.println("htmlText=>" + htmlText);
			this.jsonList.add(htmlText);

			// 取得總頁數
			JsonParser jsonParser = new JsonParser();
			JsonObject jsonObj = jsonParser.parse(htmlText).getAsJsonObject();
			String p = jsonObj.get("p").getAsString().trim();
			totalPage = Integer.valueOf(p);
			currentPage++;
		} while (currentPage <= totalPage);
	}

}
