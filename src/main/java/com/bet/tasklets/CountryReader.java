package com.bet.tasklets;

import java.util.HashSet;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bet.entity.CodeTable;
import com.github.houbb.opencc4j.util.ZhConverterUtil;

public class CountryReader implements Tasklet {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private String reqUrl;// 爬取的網址
	private Set<CodeTable> codeTableSet = new HashSet<CodeTable>();

	@Autowired
	WebDriver webDriver;

	@Value("${emptyValue}")
	private String emptyValue;
	
	public CountryReader(String reqUrl) {
		super();
		this.reqUrl = reqUrl;
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		getHtmlText(reqUrl);
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("codeTableSet",
				this.codeTableSet);

		return RepeatStatus.FINISHED;
	}

	public void getHtmlText(String reqUrl) {

		// driver.manage().window().setSize(new Dimension(1920, 1080));
		webDriver.get(reqUrl);
		By locator = By.cssSelector("body > table > tbody > tr");
		WebDriverWait wait = new WebDriverWait(webDriver, 10, 1); // 每1秒檢查元素是否存在，等待10秒直到元素可见。
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

		String html = webDriver.getPageSource();
		Document document = Jsoup.parse(html);
		//TODO add processor
		Elements trs = document.select("body > table > tbody > tr");
		System.out.println("==========爬取資料如下==========:");

		//TODO 待移至processor
		for (int i = 0; i < trs.size(); i++) {
			Element tr = trs.get(i);
			String codeId = tr.child(0).text().trim();
			String codeName = tr.child(1).text().trim();
			codeName = ZhConverterUtil.convertToTraditional(codeName);

			// 不取符合下列國家代碼的資料。
			if ("".equals(codeId)||
				"CS".equals(codeId) ||
				"TN".equals(codeId) ||
				"BB".equals(codeId) ||
				i == 0 // 不取第一列tr
			) {
				continue;
			}
			
			// 修改國家名稱
			switch (codeId) {
			case "AU":
				codeName = "澳洲";
				break;
			case "KR":
				codeName = "南韓";
				break;
			case "TW":
				codeName = "臺灣";
				break;
			case "IT":
				codeName = "義大利";
				break;
			case "ID":
				codeName = "印尼";
				break;
			}

			CodeTable codeTable = new CodeTable();
			codeTable.setCodeId(codeId);
			codeTable.setCodeName(codeName);
			codeTable.setCodeType("country");
			codeTable.setDesc(tr.child(2).text().trim());
			codeTable.setCountryId(this.emptyValue);
			codeTable.setParentCodeId(this.emptyValue);
			codeTableSet.add(codeTable);

		}
		//新增基本資料
		addBaseData("B", "球類", "BigCategory", "競技大類代碼",this.emptyValue,this.emptyValue);
		addBaseData("F", "金融", "BigCategory", "競技大類代碼",this.emptyValue,this.emptyValue);
		addBaseData("FB", "足球", "MidCategory", "競技中類代碼",this.emptyValue,this.emptyValue);
		addBaseData("TN", "網球", "MidCategory", "競技中類代碼",this.emptyValue,this.emptyValue);
		addBaseData("BK", "籃球", "MidCategory", "競技中類代碼",this.emptyValue,this.emptyValue);
		addBaseData("BB", "棒球", "MidCategory", "競技中類代碼",this.emptyValue,this.emptyValue);
		addBaseData("G1", "蘇格蘭", "country", "Scotland",this.emptyValue,this.emptyValue);
		addBaseData("G2", "英格蘭", "country", "England",this.emptyValue,this.emptyValue);
//			System.out.println(arrayList.size());

	}

	private void addBaseData(String codeId, String codeName, String codeType, String description,String countryId,String parentCodeId) {
		CodeTable codeTable = new CodeTable();
		codeTable.setCodeId(codeId);
		codeTable.setCodeName(codeName);
		codeTable.setCodeType(codeType);
		codeTable.setDesc(description);
		codeTable.setCountryId(countryId);
		codeTable.setParentCodeId(parentCodeId);
		codeTableSet.add(codeTable);
	}
}
