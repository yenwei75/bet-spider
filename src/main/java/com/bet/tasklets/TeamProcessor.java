package com.bet.tasklets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import com.bet.entity.BetResult;

import com.bet.entity.Team;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TeamProcessor implements Tasklet {

	private final Logger logger = LoggerFactory.getLogger(TeamProcessor.class);
	private List<BetResult> betResultList = new ArrayList<BetResult>();
	private Set<Team> teamSet = new HashSet<Team>();

	@Value("${emptyValue}")
	private String emptyValue;
	
	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		this.betResultList = (List<BetResult>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("betResultList");

		for (BetResult betResult : this.betResultList) {
			String bigCategory = betResult.getBigCategory();
			String teamName = betResult.getTeamName();
			String gameKind = betResult.getGameKind();// 球種
			String leagueName = betResult.getLeagueName();// 聯盟
			String cnTeamName = betResult.getCnTeamName();// 轉簡體

			Team team = new Team();
			team.setBigCategory(bigCategory);
			team.setMidCategory(gameKind);
			team.setSmallCategory(leagueName);
			team.setEngName(this.emptyValue);
			team.setTwChName(teamName);
			team.setCnChName(cnTeamName);
			this.teamSet.add(team);

		}

		ExecutionContext executionContext = chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext();
		executionContext.put("teamSet", this.teamSet);

		return RepeatStatus.FINISHED;
	}

}
