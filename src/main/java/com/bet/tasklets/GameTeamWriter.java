package com.bet.tasklets;

import com.bet.entity.GameTeam;
import com.bet.model.services.GameTeamService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class GameTeamWriter implements Tasklet {

	@Autowired
	private GameTeamService gameTeamService;

	private final Logger logger = LoggerFactory.getLogger(GameTeamWriter.class);
	private Set<GameTeam> gameTeamSet = new HashSet<GameTeam>();

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		this.gameTeamSet = (Set<GameTeam>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("gameTeamSet");

		for (GameTeam gameTeam : this.gameTeamSet) {
			gameTeamService.saveGameTeam(gameTeam);
		}
		return RepeatStatus.FINISHED;
	}

}
