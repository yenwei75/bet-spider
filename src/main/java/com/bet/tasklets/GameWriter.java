package com.bet.tasklets;

import com.bet.entity.Game;
import com.bet.model.services.GameService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

public class GameWriter implements Tasklet {

	@Autowired
	private GameService gameService;

	private final Logger logger = LoggerFactory.getLogger(GameWriter.class);
	private Set<Game> gameSet = new HashSet<Game>();

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		this.gameSet = (Set<Game>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("gameSet");

		for (Game game : this.gameSet) {
			gameService.saveGame(game);
		}
		return RepeatStatus.FINISHED;
	}

}
