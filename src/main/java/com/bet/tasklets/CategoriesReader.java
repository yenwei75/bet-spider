package com.bet.tasklets;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bet.entity.CodeTable;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CategoriesReader implements Tasklet {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	private String reqUrl;// 爬取的網址
	private Set<CodeTable> codeTableSet = new HashSet<CodeTable>();

	@Autowired
	private WebDriver webDriver;

	@Value("${bigCategoryId}")
	private String bigCategoryId;
	
	@Value("${emptyValue}")
	private String emptyValue;

	public CategoriesReader(String reqUrl) {
		super();
		this.reqUrl = reqUrl;
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		this.codeTableSet = (Set<CodeTable>) chunkContext.getStepContext().getStepExecution().getJobExecution()
				.getExecutionContext().get("codeTableSet");

		getHtmlText(reqUrl);
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("codeTableSet",
				this.codeTableSet);

		return RepeatStatus.FINISHED;
	}

	public void getHtmlText(String reqUrl) {
		webDriver.get(reqUrl);

		By locator = By.xpath("/html/body/pre");// 爬取資料的xpath路徑
		WebDriverWait wait = new WebDriverWait(webDriver, 10, 1); // 每1秒檢查元素是否存在，等待10秒直到元素可见。
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

		WebElement element = webDriver.findElement(locator);
		System.out.println("==========爬取資料如下==========:");
		String json = element.getText().trim();
		// System.out.println(json);

		// TODO add processor
		// 不需取得總頁數，因為沒有page
		JsonParser jsonParser = new JsonParser();
		JsonArray jsonArray = jsonParser.parse(json).getAsJsonArray();

		String bigCategory = "";// 大分類名
		String midCategory = "";// 中分類名
		String smallCategory = "";// 小分類名
		String parentCodeId = "";// 上一層代碼
		String countryId = "";// 國家代碼
		int count = 1;// 流水號

		   /*將3個name轉成代碼
        {
           "categoryId": "s-441",
           "name": "足球",//代碼轉換成FB
           "subCategories": [
               {
                   "categoryId": "c-2913",
                   "name": "法國",//代碼轉換成FR
                   "subCategories": [
                       {
                           "categoryId": "t-4042",
                           "name": "法國足總盃",//轉換成3碼流水號
                           "categoryOrder": 0
                       }
                   ],
                   "categoryOrder": 0
               }
           ]
       }    
       */
		
		// TODO 待移至processor
		for (int i = 0; i < jsonArray.size(); i++) {//jsonArray.size() = 1、2、3、4為球類
			JsonObject jsonObj = jsonArray.get(i).getAsJsonObject();
			JsonArray subCategories = jsonObj.get("subCategories").getAsJsonArray();

			for (int j = 0; j < subCategories.size(); j++) {
				JsonObject subCategoriesJsonObj = subCategories.get(j).getAsJsonObject();
				JsonArray subCategoriesJsonArray = subCategoriesJsonObj.get("subCategories").getAsJsonArray();

				for (int k = 0; k < subCategoriesJsonArray.size(); k++) {

					bigCategory = jsonObj.get("name").getAsString().trim();
					midCategory = subCategoriesJsonObj.get("name").getAsString().trim();
					smallCategory = subCategoriesJsonArray.get(k).getAsJsonObject().get("name").getAsString().trim();

					// 將大分類轉成codeId
					for (CodeTable codeTable : this.codeTableSet) {
						if (bigCategory.equals(codeTable.getCodeName())) {
							parentCodeId = codeTable.getCodeId();
							break;
						}else {
							parentCodeId = this.emptyValue;
						}

					}
					// 將中分類轉成codeId
					for (CodeTable codeTable : this.codeTableSet) {
						if (midCategory.equals(codeTable.getCodeName())) {
							countryId = codeTable.getCodeId();
							break;
						}else {
							countryId = this.emptyValue;
						}
					}
					System.out.println("【" + bigCategory + "】 【" + midCategory + "】【" + smallCategory + "】");

					CodeTable codeTable = new CodeTable();
					codeTable.setCodeType("SmallCategory");
					codeTable.setCodeName(smallCategory);
					codeTable.setDesc("競技小類代碼");
					codeTable.setParentCodeId(parentCodeId);
					codeTable.setCountryId(countryId);
					String codeId = this.bigCategoryId + parentCodeId + countryId + String.format("%03d", count);
					codeTable.setCodeId(codeId);
					this.codeTableSet.add(codeTable);
//					System.out.println(codeTable.toString());
					count++;
				}
			}
		}
	}
}
