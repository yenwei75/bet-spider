package com.bet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BetSpiderApplication {

	public static void main(String[] args) {
		SpringApplication.run(BetSpiderApplication.class, args);
	}

}
